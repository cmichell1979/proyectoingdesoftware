package application;
	

import java.io.InputStream;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) {

        VBox Arriba = new VBox();

       
        
        Label labelTitle = new Label("Iniciar Sesi�n");
        labelTitle.setFont(new Font(40));
        
        VBox inicio = new VBox();
        Label lbl1 = new Label("Credenciales de Estudiante");
        lbl1.setFont(new Font(20));
       
        Label lbl2 = new Label("Usuario");
       
        lbl2.setFont(new Font(20));
        
        Label lbl3 = new Label("Contrase�a");
        lbl3.setFont(new Font(20));

        TextField txt1 = new TextField();
        txt1.setFont(new Font(18));
        
        txt1.setPromptText("Ingrese su usuario");
       
        txt1.setPrefWidth(241);
        txt1.setPrefHeight(44);
        
        PasswordField txt2 = new PasswordField();
        txt2.setFont(new Font(18));
        txt2.setPromptText("Ingrese su contrase�a");        
        txt2.setPrefWidth(241);
        txt2.setPrefHeight(44);        
        
        Button btnLogin = new Button("Ingresar");
        btnLogin.setFont(new Font(20));
        btnLogin.setPrefWidth(370);
        btnLogin.setPrefHeight(44);                
        btnLogin.setMaxWidth(Double.MAX_VALUE);
        btnLogin.setCursor(Cursor.HAND);

        
        inicio.getChildren().addAll(lbl1,lbl2,txt1, lbl3, txt2,btnLogin );
        inicio.setAlignment(Pos.TOP_LEFT);
        inicio.setBackground(new Background(new BackgroundFill(Color.web("#DFE4E3"), CornerRadii.EMPTY, Insets.EMPTY)));

        
        VBox.setMargin(lbl2, new Insets(10, 10, 10, 10));
        VBox.setMargin(lbl3, new Insets(10, 10, 10, 10));
        VBox.setMargin(btnLogin, new Insets(20, 20, 20, 20));
        
        Arriba.getChildren().addAll(labelTitle, inicio);
        Arriba.setPrefWidth(422);
        Arriba.setAlignment(Pos.CENTER);
        Arriba.setBackground(new Background(new BackgroundFill(Color.web("#2DBDB1"), CornerRadii.EMPTY, Insets.EMPTY)));
        VBox.setMargin(inicio, new Insets(00, 30, 30, 30));        
        
        VBox abajo = new VBox(40);        
        
          
        ImageView imageLogo;

       
        InputStream inputStream;
        inputStream =  getClass().getResourceAsStream("/images/logo.png");
        Image image = new Image(inputStream);
           
        imageLogo = new ImageView(image);
        abajo.getChildren().add(imageLogo);
        
        abajo.setAlignment(Pos.CENTER);
        abajo.setBackground(new Background(new BackgroundFill(Color.web("#2F4557"), CornerRadii.EMPTY, Insets.EMPTY)));
               
        VBox root = new VBox();
        root.getChildren().addAll(Arriba, abajo);
        HBox.setHgrow(Arriba, Priority.ALWAYS);
        HBox.setHgrow(abajo, Priority.ALWAYS);
        
        Scene scene = new Scene(root, 500, 600);
        
        primaryStage.setTitle("Pantalla de Acceso");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
